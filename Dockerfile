# Use the official Loki image from Grafana Labs
FROM grafana/loki:latest

# Expose the port Loki runs on
EXPOSE 3100

# Specify the configuration file
COPY loki-config.yaml /etc/loki/config.yaml
